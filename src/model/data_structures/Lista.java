package model.data_structures;

import model.vo.Taxi;

public class Lista<T extends Comparable <T>> implements LinkedList<T>
{

	private Node<T> cabeza ;

	private Node<T> actual ;

	private Node<T> ultimo ;

	public Lista()
	{
		cabeza = null ;

		actual= cabeza ;

		ultimo = null ;

	}


	@Override
	public void add(T bag)
	{
		if(cabeza == null )
		{
			cabeza = new Node(bag) ;
			ultimo = cabeza ;
		}
		else 
		{
			Node<T> nuevo = new Node<T>(bag) ;
			ultimo.cambiarSig(nuevo);
			ultimo = nuevo ;

		}	
	}


	@Override
	public void delete(T bag) 
	{
		if(cabeza != null )
		{
			boolean vari = true ;
			if(cabeza.darT().equals(bag))
			{
				cabeza = cabeza.darSig() ;
			}
			else if(ultimo.darT().equals(bag))
			{
				ultimo = null ;
			}
			else
			{	listing();
			while (vari)
			{

				if(actual.darSig().darT().equals(bag))
				{
					actual.cambiarSig(actual.darSig().darSig());
					vari = false;
				}
				else
					avanzar();

			}
			}

		}
	}


	@Override
	public T getElement(T bag) 
	{
		T result = null;

		boolean vari = true;

		if(cabeza!=null)
		{
			listing(); 
			while(vari)
			{
				avanzar();
				if(getCurrent().equals(bag))
				{
					result = getCurrent() ;
					vari = false ;
				}

			}

		}
		return result;
	}



	@Override
	public int size() 
	{
		int variable = 0 ;

		if(cabeza!=null)
		{
			listing();
			while(actual!=null)
			{
				avanzar();
				variable ++ ;
			}
		}

		return variable;
	}



	@Override
	public void listing() 
	{
		actual = cabeza ;

	}


	@Override
	public boolean next() 
	{
		return actual.darSig() != null? true : false;

	}

	public void avanzar()
	{
		actual = actual.darSig() ;
	}


	@Override
	public T get(T bag) 
	{
		return null ;
	}


	@Override
	public T getCurrent() 
	{

		return actual.darT();
	}
	public Node<T> getActual()
	{
		return actual ;
	}
}
