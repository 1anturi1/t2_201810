package model.data_structures;

import org.omg.CORBA.Context;
import org.omg.CORBA.ContextList;
import org.omg.CORBA.DomainManager;
import org.omg.CORBA.ExceptionList;
import org.omg.CORBA.NVList;
import org.omg.CORBA.NamedValue;
import org.omg.CORBA.Object;
import org.omg.CORBA.Policy;
import org.omg.CORBA.Request;
import org.omg.CORBA.SetOverrideType;

public class Node <T> implements Comparable<Node>
{

	private Node<T> sig ;

	private T info ;



	public Node(  T pinfo)
	{
		sig =null ;
		info = pinfo ;
	}

	
	public Node<T> darSig()
	{
		return sig ;
	}



	public void cambiarSig(Node<T> bag)
	{
		sig =  bag ;
	}


	public T darT()
	{
		return info ;
	}
	

	public void cambiarT(T param)
	{
		info = param ;
	}

	

	@Override
	public int compareTo(Node o) 
	{
		
		
		return 0;
	}



}
