package model.logic;

import api.ITaxiTripsManager;


import model.vo.Taxi;
import model.vo.Service;
import model.data_structures.LinkedList;
import model.data_structures.Lista;

import java.io.FileNotFoundException;
import java.io.FileReader;

import com.google.gson.JsonArray;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

public class TaxiTripsManager implements ITaxiTripsManager 
{


	private Lista<Service> listServices ;

	private Lista<Taxi> listTaxis;


	public void loadServices (String serviceFile)
	{
		listServices = new Lista<Service>();
		listTaxis = new Lista<Taxi>();

		JsonParser parser = new JsonParser();

		try 
		{
			String taxiTripsDatos = serviceFile;

			/* Cargar todos los JsonObject (servicio) definidos en un JsonArray en el archivo */

			JsonArray arr= (JsonArray) parser.parse(new FileReader(taxiTripsDatos));

			/* Tratar cada JsonObject (Servicio taxi) del JsonArray */
			for (int i = 0; arr != null && i < arr.size(); i++)
			{
				JsonObject obj= (JsonObject) arr.get(i);
				/* Mostrar un JsonObject (Servicio taxi) */
				System.out.println("------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
				System.out.println(obj);
				//-----------------------------------------
				//Crea la lista de servicios
				//------------------------------------------
				String tripId = "NaN";

				if ( obj.get("trip_id") != null )
				{ 
					tripId = obj.get("trip_id").getAsString(); 
				}

				String taxiId = "NaN";
				if ( obj.get("taxi_id") != null )
				{ 
					tripId = obj.get("taxi_id").getAsString(); 
				}

				int tripSeconds = 0;
				if ( obj.get("trip_seconds") != null )
				{ 
					tripId = obj.get("trip_seconds").getAsString(); 
				}

				double tripMiles = 0 ;				
				if ( obj.get("trip_miles") != null )
				{ 
					tripId = obj.get("trip_miles").getAsString(); 
				}

				double tripTotal =0 ;
				if ( obj.get("trip_total") != null )
				{ 
					tripId = obj.get("trip_total").getAsString(); 
				}

				int community = 0 ;
				if ( obj.get("pickup_community_area") != null )
				{ 
					tripId = obj.get("pickup_community_area").getAsString(); 
				}

				Service ser = new Service(tripId, taxiId, tripSeconds, tripMiles, tripTotal, community);

				listServices.add(ser);
				//--------------------------------------------------
				//Crea la lista de taxis
				//--------------------------------------------
				String company = "NaN";
				if ( obj.get("company") != null )
				{ 
					tripId = obj.get("company").getAsString(); 
				}
				Taxi tax = new Taxi(company, taxiId);
				listTaxis.add(tax);

			}

		}
		catch (JsonIOException e1 ) 
		{
			e1.printStackTrace();
		}
		catch (JsonSyntaxException e2) 
		{

			e2.printStackTrace();
		}
		catch (FileNotFoundException e3) 
		{

			e3.printStackTrace();
		}




		System.out.println("Inside loadServices with " + serviceFile);
	}

	@Override
	public LinkedList<Taxi> getTaxisOfCompany(String company)
	{
		LinkedList<Taxi> nueva = null ;
		boolean centi = true;

		System.out.println("Inside getTaxisOfCompany with " + company);
		listTaxis.listing();
		while(centi)
		{
			Taxi el = listTaxis.getCurrent();
			if(el==null)
			{
				centi = false ;
			}
			if (el.getCompany().equalsIgnoreCase(company))
			{
				nueva.add(el);
			}
			listTaxis.avanzar();
		}

		return nueva;
	}

	@Override
	public LinkedList<Service> getTaxiServicesToCommunityArea(int communityArea) {
		// TODO Auto-generated method stub
		System.out.println("Inside getTaxiServicesToCommunityArea with " + communityArea);
		return null;
	}


}
