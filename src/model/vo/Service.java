package model.vo;

/**
 * Representation of a Service object
 */
public class Service implements Comparable<Service> {

	
	
	private String tripId;
	
	private String taxiId ;
	
	private int tripSeconds ;
	
	private double tripMiles ;
	
	private double tripTotal ;
	
	private int communityArea;
	
	public Service(String ptripId, String pTaxiId, int pTripSeconds, double ptripMiles, double pTripTotal, int pCommunity)
	{
		tripId = ptripId;
		taxiId = pTaxiId ;
		tripSeconds = pTripSeconds ;
		tripMiles = ptripMiles ;
		tripTotal = pTripTotal ;
		communityArea =pCommunity ;
		
	}
	
	
	
	/**
	 * @return id - Trip_id
	 */
	public String getTripId() 
	{
		return tripId;
	}	
	
	/**
	 * @return id - Taxi_id
	 */
	public String getTaxiId() 
	{

		return taxiId;
	}	
	
	/**
	 * @return time - Time of the trip in seconds.
	 */
	public int getTripSeconds() 
	{
		return tripSeconds;
	}

	/**
	 * @return miles - Distance of the trip in miles.
	 */
	public double getTripMiles() 
	{
		return tripMiles;
	}
	
	/**
	 * @return total - Total cost of the trip
	 */
	public double getTripTotal() 
	{
		return tripTotal;
	}

	public int getCommunityArea()
	{
		return communityArea ;
	}
	@Override
	public int compareTo(Service o) 
	{
		int variable = 0;
		
		if(communityArea == o.communityArea)
			return variable ;
		
		else if(communityArea > o.communityArea)
		{
			variable = 1 ;
		}
		else if(communityArea < o.communityArea)
		{
			variable = -1 ;
		}
	
		return variable ;
	}
}
