package model.vo;

/**
 * Representation of a taxi object
 */
public class Taxi implements Comparable<Taxi>
{

	private String company ;
	private String taxiId ;

	public Taxi (String pCompany, String pTaxiId)
	{
		company =pCompany ;
		taxiId = pTaxiId ;
	}
	
	
	/**
	 * @return id - taxi_id
	 */
	public String getTaxiId() 
	{
		return taxiId;
	}

	
	/**
	 * @return company
	 */
	public String getCompany() 
	{
		return company;
	}

	
	@Override
	public int compareTo(Taxi o) 
	{
		int variable = 0;

		if(company.compareTo(o.getCompany()) > 0)
		{
			variable =1 ;
		}

		else if(company.compareTo(o.getCompany()) < 0)
		{
			variable = -1 ;
		}

		return variable ;
	}	
}
